import os.path
import re

import tldextract
from bs4 import BeautifulSoup as Soup
from tqdm import tqdm
from xdriver.XDriver import XDriver
from xdriver.xutils.Regexes import Regexes
from collections import defaultdict
from selenium.common.exceptions import TimeoutException
import pandas as pd
import numpy as np
# + :one or more repetition,
# ? :zero or one repetition
# * :zero or more repetition
# LOGIN = r"(log|sign)([^0-9a-zA-Z]|\s)*(in|on)|authenticat(e|ion)|/(my([^0-9a-zA-Z]|\s)*)?(user|account|profile|dashboard)"
# SIGNUP = r"sign([^0-9a-zA-Z]|\s)*up|regist(er|ration)?|(create|new)([^0-9a-zA-Z]|\s)*(new([^0-9a-zA-Z]|\s)*)?(acc(ount)?|us(e)?r|prof(ile)?)"
# SSO = r"[^0-9a-zA-Z]+sso[^0-9a-zA-Z]+|oauth|openid"
# AUTH = r"%s|%s|%s|auth|(new|existing)([^0-9a-zA-Z]|\s)*(us(e)?r|acc(ount)?)|account|connect|profile|dashboard" % (LOGIN, SIGNUP, SSO)

def rel2abs(html_path):

    soup = Soup(open(html_path, encoding='iso-8859-1').read(), features="lxml")
    if not soup.find('base'):
        head = soup.find('head')
        if head is not None:
            head = next(head.children, None)
            base = soup.new_tag('base')
            base['href'] = os.path.basename(html_path).split('.html')[0]
            head.insert_before(base)
    return soup.prettify()

# def multiple_replace():
#     # Create a regular expression  from the dictionary keys
#     dict = {
#         "(?i)(log|sign)([^0-9a-zA-Z]|\s)*(in|on)|authenticat(e|ion)|(connect)": "l0gme_in",
#         "(?i)sign([^0-9a-zA-Z]|\s)*up|regist(er|ration)?|(create|new)([^0-9a-zA-Z]|\s)*(new([^0-9a-zA-Z]|\s)*)?(acc(ount)?|us(e)?r|prof(ile)?)": "signme_up",
#         "(?i)(my([^0-9a-zA-Z]|\s)*)?(user|account|profile|dashboard)":  "acc0unt",
#         "(?i)[^0-9a-zA-Z]+sso[^0-9a-zA-Z]+|oauth|openid|auth":  "myss0",
#     }
#     regex = [re.compile(x) for x in dict.keys()]
#     mapped_str = [x for x in dict.values()]
#     return regex, mapped_str
#

class TestObfuscation():
    def __init__(self, base_dir, result_file):
        self.base_dir = base_dir
        self.result_file = result_file

    @staticmethod
    def url_obfuscation(url_str):
        url_str = re.sub("(?i)(log|sign)(_-)*(in|on|up)|(regist)", "enter", url_str)
        url_str = re.sub("(?i)account", 'acc0unt', url_str)
        url_str = url_str.replace('authen', 'verify').replace('connect', 'c0nnect')
        url_str = url_str.replace('profile', 'pr0file').replace('dashboard', 'dashb0ard')
        return url_str

    def crp_detector_test(self, obfuscate=False, start_index=0):
        xd = XDriver.boot(chrome=True)
        # html_path = 'https://www.amazon.com/ap/signin?_encoding=UTF8&openid.assoc_handle=usflex&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.mode=checkid_setup&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&openid.ns.pape=http%3A%2F%2Fspecs.openid.net%2Fextensions%2Fpape%2F1.0&openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.com%2Fgp%2Fcss%2Fhomepage.html%3Fie%3DUTF8%26%252AVersion%252A%3D1%26%252Aentries%252A%3D0'
        for it, file in tqdm(enumerate(os.listdir(self.base_dir.split("file://")[-1]))):
            if it <= start_index:
                continue
            if os.path.exists(self.result_file):
                if file.split('.html')[0] in open(self.result_file).read():
                    continue
            # add a <base> tag
            html_path = os.path.join(self.base_dir, file)
            soup = rel2abs(html_path.split('file://')[-1])
            with open(html_path.split('file://')[-1], "w", encoding='utf-8') as f:
                f.write(str(soup))

            try:
                xd.crawl_init(html_path, bfs=True, depth=1, follow=[Regexes.AUTH])  # set crawling config
                xd.crawl_next()  # jump from google to the target website

                if obfuscate:
                    xd._invoke(xd.execute_script, "return change_password_type();")
                    xd._invoke(xd.execute_script, "return form_obfuscation();")

                login_forms, signup_forms = xd.get_account_forms()
                if login_forms or signup_forms:
                    crp = 'credential'
                else:
                    crp = 'noncredential'
            except TimeoutException:
                crp = 'noncredential'

            with open(self.result_file, 'a+') as f:
                f.write(file.split('.html')[0] + '\t' + crp)
                f.write('\n')

            # xd.reboot()

    def crp_locator_test(self, obfuscate=False, start_index=0):
        xd = XDriver.boot(chrome=True)
        for it, file in tqdm(enumerate(os.listdir(self.base_dir.split("file://")[-1]))):
            if it <= start_index:
                continue
            if os.path.exists(self.result_file):
                if file.split('.html')[0] in open(self.result_file).read():
                    continue
            # add a <base> tag
            html_path = os.path.join(self.base_dir, file)
            soup = rel2abs(html_path.split('file://')[-1])
            with open(html_path.split('file://')[-1], "w", encoding='utf-8') as f:
                f.write(str(soup))

            xd.crawl_init(html_path, bfs = True, depth = 1, follow = [Regexes.AUTH]) # set crawling config
            xd.crawl_next() # jump from google to the target website
            internal_links = xd.get_internal_links() # get all internal links

            # get urls that match with regex heuristic
            match = []
            for link in internal_links:
                match_this = True if re.search(Regexes.AUTH, link.split(self.base_dir)[-1], re.IGNORECASE) else False
                if match_this:
                    if obfuscate:
                        if self.base_dir in link:
                            link = self.base_dir + self.url_obfuscation(link.split(self.base_dir)[-1])
                        else:
                            link = self.url_obfuscation(link)
                        match_this = True if re.search(Regexes.AUTH, link.split(self.base_dir)[-1],
                                                       re.IGNORECASE) else False
                        if match_this:
                            match.append(link)
                    else:
                        match.append(link)


            with open(self.result_file, 'a+') as f:
                for url in set(match):
                    f.write(file.split('.html')[0] + '\t' + url)
                    f.write('\n')

            # xd.reboot()

    def results_collect(self, gt_file):

        gt_domains = [x.split('\t')[0].split('://')[1] if x.split('\t')[0].startswith('http') else x.split('\t')[0] for x in open(gt_file).readlines()[1:]]
        gt_login = []
        for x in open(gt_file).readlines()[1:]:
            domain = x.split('\t')[0].split('://')[1] if x.split('\t')[0].startswith('http') else x.split('\t')[0]
            x = x.split('\t')[-1]
            if self.base_dir in x.split('\t')[-1]: # change file:/// to https://
                x = x.split(self.base_dir)[1]
                x = 'https://' + domain + '/' + x
            gt_login.append(x)

        total_ct = len(os.listdir(self.base_dir.split('file://')[1]))
        temp = defaultdict(list)
        for key, value in zip(gt_domains, gt_login):
            temp[key].append(value)

        alias_domains = [tldextract.extract(x).domain for x in gt_login]
        for key, value in zip(alias_domains, gt_login):
            temp[key].append(value)
            if key == 'clicknupload.org':
                temp['clicknupload.com'].append(value)

        alias_domains = ['.'.join([tldextract.extract(x).domain, tldextract.extract(x).suffix]) for x in gt_login]
        for key, value in zip(alias_domains, gt_login):
            temp[key].append(value)

        alias_domains = ['.'.join([tldextract.extract(x).subdomain, tldextract.extract(x).domain, tldextract.extract(x).suffix]) for x in gt_login]
        for key, value in zip(alias_domains, gt_login):
            temp[key].append(value)

        alias_domains = ['.'.join(['www', tldextract.extract(x).domain]) for x in gt_domains]
        for key, value in zip(alias_domains, gt_login):
            temp[key].append(value)

        visited_domains = []
        printed_domains = []
        correct_ct = 0
        print('Total reported domains: ', len(set([x.split('\t')[0] for x in open(self.result_file).readlines()])))
        for line in open(self.result_file).readlines():
            report_login = line.split('\t')[-1]
            domain = line.split('\t')[0]
            if domain in visited_domains:
                continue

            if any([True if report_login in x else False in x for x in temp.values()]):
                correct_ct += 1
                visited_domains.append(domain)
                if domain in printed_domains: printed_domains.remove(domain)

            elif any([True if re.search(x, report_login) else False for x in
                         ['login', 'signup', 'signin', 'Login', 'LogIn', 'Signup', 'SignUp',
                         'SignIn', 'Signin', 'registration', 'Registration', 'sign_up', 'sign-up',
                         'sign-in', 'sign_in', 'log_in', 'log-in', 'register', 'Register', 'logon']]):
                correct_ct += 1
                visited_domains.append(domain)
                if domain in printed_domains: printed_domains.remove(domain)
            else:
                if domain in printed_domains:
                    continue

                printed_domains.append(domain)
            # domain = line.split('\t')[0]
            # if domain in visited_domains:
            #     continue
            # if domain in temp.keys():
            #     if report_login in temp[domain]:
            #         correct_ct += 1
            #         visited_domains.append(domain)
            #     else:
            #         print(domain)
            # else:
            #     domain = '.'.join([tldextract.extract(line.split('\t')[0]).domain,
            #                        tldextract.extract(line.split('\t')[0]).suffix])
            #     if domain in visited_domains:
            #         continue
            #     if domain in temp.keys():
            #         if report_login in temp[domain]:
            #             correct_ct += 1
            #             visited_domains.append(domain)
            #         else:
            #             print(domain)
            #     else:
            #         domain = tldextract.extract(line.split('\t')[0]).domain + '.com'
            #         if domain in visited_domains:
            #             continue
            #         if domain in temp.keys():
            #             if report_login in temp[domain]:
            #                 correct_ct += 1
            #                 visited_domains.append(domain)
            #             else:
            #                 print(domain)
            #         else:
            #             print("gt file doesnt have label for {}".format(domain))
            #             continue

        for domain in printed_domains:
            print('Fail: ', domain)
        return correct_ct, total_ct

def crp_result_collect(orig_dir, html_dir, result_file, gt_file='val_merge_coords.txt'):
    predicted_result = []
    gt_result = []
    result = pd.read_table(result_file, header=None)
    result.columns = ['domain', 'predict_crp']
    gt_file = pd.read_table(gt_file, header=None)
    gt_file.columns = ['folder', 'coords', 'type', 'crp_label']

    for folder in os.listdir(orig_dir):
        url = open(os.path.join(orig_dir, folder, 'info.txt')).read()
        try:
            url = eval(url)['url']
        except:
            url = url

        try:
            crp = list(gt_file.loc[gt_file['folder'] == folder]['crp_label'])[0]
        except IndexError:
            continue

        if os.path.exists(os.path.join(html_dir, 'credential', '.'.join([tldextract.extract(url).domain, tldextract.extract(url).suffix])+'.html')) or \
                os.path.exists(os.path.join(html_dir, 'noncredential', '.'.join([tldextract.extract(url).domain, tldextract.extract(url).suffix])+'.html')):
            domain = '.'.join([tldextract.extract(url).domain, tldextract.extract(url).suffix])
            try:
                predicted_result.append(list(result.loc[result['domain'] == domain]['predict_crp'])[0])
            except IndexError:
                predicted_result.append('noncredential') # predict as noncredential by default
        else:
            predicted_result.append('noncredential')  # cannot get html somehow, predict as noncredential by default
        gt_result.append(crp)

    return gt_result, predicted_result


if __name__ == '__main__':
    # soup = rel2abs('file:///Users/liuruofan/PycharmProjects/xdriver3-open/1003_legitimate_contents/460_legitimate_htmls/about.gitlab.com.html'.split('file://')[-1])
    # with open('file:///Users/liuruofan/PycharmProjects/xdriver3-open/1003_legitimate_contents/460_legitimate_htmls/about.gitlab.com.html'.split('file://')[-1], "w", encoding='utf-8') as file:
    #     file.write(str(soup))
    # print(soup[:300])
    # regex, mapped_str = multiple_replace()
    # matched_keys = []
    # ct = 0
    # for file in tqdm(os.listdir('1003_legitimate_contents/460_legitimate_htmls')):
    #     html = open(os.path.join('1003_legitimate_contents/460_legitimate_htmls', file), encoding='iso-8859-1').read()
    #     vis_text = Soup(html, features="lxml").get_text()
    #     vis_text = keyword_obfuscation(vis_text, regex, mapped_str)
    #     m = re.search(AUTH, vis_text, re.IGNORECASE)
    #     if m:
    #         matched_keys.append(m.group(0))
    #         ct += 1
    #         print(ct)
    #         print(m.group(0))
    # print(ct)
    # print(set(matched_keys))
    # m = re.search(AUTH, "newly joined user", re.IGNORECASE)
    # print(m)

    # base_dir = "file://D:/ruofan/xdriver3-open/val_merge_htmls/noncredential"
    # result_file = "val_noncredential_obfuscate.txt"
    # test = TestObfuscation(base_dir, result_file)
    # test.crp_detector_test(obfuscate=True, start_index=1508)

    # base_dir = "file://D:/ruofan/xdriver3-open/val_merge_htmls/noncredential"
    # result_file = "val_noncredential.txt"
    # test = TestObfuscation(base_dir, result_file)
    # test.crp_detector_test(obfuscate=False, start_index=1376)

    # base_dir = "file://D:/ruofan/xdriver3-open/600_legitimate_contents_full/600_legitimate_contents_full"
    # result_file = "600_reporturls_obfuscate.txt"
    # test = TestObfuscation(base_dir, result_file)
    # test.crp_locator_test(obfuscate=True, start_index=0)

    gt_result, predicted_result = crp_result_collect(orig_dir='val_merge_folder', html_dir='val_merge_htmls',
                                                     result_file='val_crpresult_collect_obfuscate.txt',
                                                     gt_file='val_merge_coords.txt')
    assert len(gt_result) == len(predicted_result)
    print('Classificaiton acc:', np.sum(np.asarray(gt_result) == np.asarray(predicted_result)) / len(gt_result))
    print(set(predicted_result))
    print('Recall:', np.sum((np.asarray(gt_result) == np.asarray(predicted_result)) & (np.asarray(gt_result) == 'credential')) / np.sum(np.asarray(gt_result) == 'credential') )
    print('Precision:', np.sum((np.asarray(gt_result) == np.asarray(predicted_result)) & (np.asarray(gt_result) == 'credential')) / np.sum(np.asarray(predicted_result) == 'credential') )











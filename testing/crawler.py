#!/usr/bin/python

import sys
sys.path.insert(0, "..")

from xdriver.XDriver import XDriver
from xdriver.xutils.Regexes import Regexes

if __name__ == "__main__":
	xd = XDriver.boot(chrome = True)

	login_urls = []
	signup_urls = []

	# target = "nus.edu.sg"
	# target = "https://www.amazon.com/ap/signin?openid.pape.max_auth_age=0&openid.return_to=https%3A%2F%2Fwww.amazon.com%2FSign-up%2Fs%3Fk%3DSign%2Bup%26ref_%3Dnav_ya_signin&openid.identity=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.assoc_handle=usflex&openid.mode=checkid_setup&openid.claimed_id=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0%2Fidentifier_select&openid.ns=http%3A%2F%2Fspecs.openid.net%2Fauth%2F2.0&"
	# target = 'file:///Users/liuruofan/PycharmProjects/xdriver3-open/1003_legitimate_contents/460_legitimate_htmls/www.appfolio.com.html'
	target = 'https://about.gitlab.com'
	''' Configure crawl
	'''
	xd.crawl_init(target, bfs = True, depth = 1, follow = [Regexes.AUTH])

	xd.crawl_next()  # jump from google to the target website
	internal_links = xd.get_internal_links()  # get all internal links
	print(internal_links)

	# Crawl loop
	# while xd.crawl_next():
	# 	login_forms, signup_forms = xd.get_account_forms()
	#
	# 	if login_forms: login_urls.append(xd.current_url())
	# 	if signup_forms: signup_urls.append(xd.current_url())
	#
	# 	if login_urls and signup_urls: break
	#
	# print("Login urls: %s" % login_urls)
	# print("Signup urls: %s" % signup_urls)

	xd.quit()
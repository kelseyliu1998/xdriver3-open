#!/usr/bin/python

import json

from xdriver.XDriver import XDriver

if __name__ == "__main__":	
	XDriver.enable_security_checks()
	driver = XDriver.boot(firefox = True)

	target = "http://instagram.com"

	''' If you want to evaluate all headers, simply pass `all = True`.
		CORS evaluation requires multiple requests so it might take some time; you need to disable it explicity if not needed '''
	ret = driver.evaluate_policies(target, all = True, cors = False)
	# ret = driver.evaluate_policies(target, hsts = True, csp = True) # Specific mechanisms
	
	print(json.dumps(ret, indent = 4))

	s = input("Exit..")

	driver.quit()
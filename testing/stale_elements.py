#!/usr/bin/python

from xdriver.XDriver import XDriver

if __name__ == "__main__":
	xd = XDriver.boot(chrome = True)

	target = "reddit.com"

	xd.get(target)
	third_party_scripts = xd.get_third_party_scripts()
	print(third_party_scripts)

	''' Reboot the browser to make *all* elements collected so far stale.
		This is only to deliberately cause multiple StaleElementReference exceptions
		and demonstrate the error handling mechanism. However, in practice such exceptions
		can occur whenever and cannot be predicted, thus the need for such a mechanism. '''
	xd.reboot()

	xd.get(target)
	for script in third_party_scripts:
		print(xd.get_attribute(script, "src")) # This causes such an exception for each of the scripts, but is handled internally by XDriver

	s = input("Exit..")
	xd.quit()
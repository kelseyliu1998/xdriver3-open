#!/usr/bin/python

import json

from xdriver.XDriver import XDriver

if __name__ == "__main__":
	''' Enable proxy before boot
	'''
	XDriver.enable_internal_proxy()
	xd = XDriver.boot(chrome = True)

	test_url = "reddit.com"
	
	flow = xd.get_redirection_flow(test_url)

	print(json.dumps(flow, indent = 4))
	xd.quit()

#!/usr/bin/python

from xdriver.XDriver import XDriver
from xdriver.xutils.Regexes import Regexes

from time import sleep

if __name__ == "__main__":
	xd = XDriver.boot(chrome = True)

	target = "https://reg.ebay.com/reg/PartialReg"
	xd.get(target)
	sleep(2)

	signup_forms = xd.get_signup_forms()
	sf = signup_forms[0] if signup_forms else None
	if not sf:
		print("Did not locate signup form..")
	else:
		''' If no `override_rules` are provided, XDriver will generate new ones. To see what rules you can set see `../xutils/forms/FormElement.py`
		'''
		# xd.fill_and_submit(sf, override_rules = {Regexes.EMAIL : "myemail@example.com", Regexes.USERNAME : "myusername", Regexes.PASSWORD : "abc123"})
		xd.fill_and_submit(sf)

	s = input("Exit..")
	xd.quit()

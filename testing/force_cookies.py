#!/usr/bin/python

from xdriver.XDriver import XDriver

if __name__ == "__main__":
	XDriver.enable_proxy(port = 8989) # Enable your own proxy to watch the traffic and do your own stuff
	XDriver.enable_internal_proxy() # XDriver's dedicated proxy; will need it to force the cookies on the wire
	xd = XDriver.boot(chrome = True)

	xd.force_cookies(rules = [{"domain" : ".facebook.com",
							   "cookies" : "XDRIVER_COOKIE=abc123456; mycookie=76;"}])

	xd.get("facebook.com")
	xd.quit()
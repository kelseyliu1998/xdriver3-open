#!/usr/bin/python

from xdriver.XDriver import XDriver

if __name__ == "__main__":
	''' Setup browser before booting.
		Some options are enabled by default, such as:
		no_ssl_errors, disable_notifications, maximized, no_default_browser_check, disable_cache.
		These have to be explicitly disabled. '''
	XDriver.enable_browser_args(profile = "mycustomprofile", disable_cache = False)
	# XDriver.enable_browser_args(virtual = True) # If you don't want to / cant't setup the graphical output
	
	''' Choose whatever browser you need
		xd = XDriver.boot(firefox = True)
		xd = XDriver.boot(opera = True)  '''
	xd = XDriver.boot(chrome = True)

	''' Navigate to the target website and collect all third-party scripts.
	'''
	target = "reddit.com"
	if not xd.get(target):
		print("Could not get %s" % target)
	else:
		scripts = xd.get_third_party_scripts()
		print("Collected %s third-party scripts" % len(scripts))


	''' Gracefully shutdown the browser and all underlying processes (e.g. mitmdump, xvfb etc.),
		delete the browser profile and any runtime files needed '''
	xd.quit()
	# xd.quit(delete_profile = False) # Keep the profile
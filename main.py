# !/usr/bin/python

import sys

sys.path.insert(0, "..")

from xdriver.XDriver import XDriver
from xdriver.xutils.Regexes import Regexes
import os
from tqdm import tqdm
import time

if __name__ == "__main__":
    '''
        Step1: Create a chromedriver
        Step2: traverse through the HTML elements, find suspicious <forms> 
        Step3: find login/signup related links from forms
    '''
    xd = XDriver.boot(chrome=True)
    result_file = '600_legitimate_loginURL.txt'

    for it, folder in tqdm(enumerate(os.listdir(os.path.join('1003_legitimate_loginbutton_labelled', '600_legitimate')))):
        if it <= 303:
            continue
        if os.path.exists(result_file):
            if folder in open(result_file).read():
                continue
        ''' Configure crawl'''
        target = open(os.path.join('1003_legitimate_loginbutton_labelled', '600_legitimate', folder, 'info.txt')).read()
        # target = "amazon.com"
        xd.crawl_init(target, bfs=True, depth=1, follow=[Regexes.AUTH])

        login_urls = []
        signup_urls = []

        # Crawl loop
        start_time = time.time()
        ct = 0
        while xd.crawl_next():
            ct += 1
            login_forms, signup_forms = xd.get_account_forms()

            if login_forms: login_urls.append(xd.current_url())
            if signup_forms: signup_urls.append(xd.current_url())

            if login_urls and signup_urls: break
            elif ct >= 10: break # too many times

        print(login_urls, signup_urls)

        with open(result_file, 'a+') as f:
            for url in set(login_urls).union(set(signup_urls)):
                f.write(folder+'\t'+url+'\t'+str(time.time()-start_time))
                f.write('\n')

        xd.reboot()
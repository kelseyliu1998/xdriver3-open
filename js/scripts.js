////// Kudos to SSOScan for the scripts! /////
//      _onTopLayer_func_script             //
//      _isChildElement_func_script        //
////////////////////////////////////////////

// Override alerts
window.alert = null;

onTopLayer = function(ele){
    if (!ele) return false;
	var document = ele.ownerDocument;
	var inputWidth = ele.offsetWidth;
	var inputHeight = ele.offsetHeight;
	/*if (inputWidth >= screen.availWidth/3 || inputHeight >= screen.availHeight/4) return false;*/
	if (inputWidth <= 0 || inputHeight <= 0) return false;
    var position = ele.getClientRects()[0];
    var j = 0;
    var score = 0;
    position.top = position.top - window.pageYOffset;
	position.left = position.left - window.pageXOffset;
    var maxHeight = (document.documentElement.clientHeight - position.top > inputHeight)? inputHeight : document.documentElement.clientHeight - position.top;
	var maxWidth = (document.documentElement.clientWidth > inputWidth)? inputWidth : document.documentElement.clientWidth - position.left;
	for (j = 0; j < 10; j++){
    	score = isChildElement(ele,document.elementFromPoint(position.left+1+j*maxWidth/10, position.top+1+j*maxHeight/10)) ? score + 1 : score;
    }
    if (score >= 5) return true;
	else return false;
}

isChildElement = function(parent, child){
	if (child == null) return false;
	if (parent == child) return true;
	if (parent == null || typeof parent == 'undefined') return false;
	if (parent.children.length == 0) return false;
	var i = 0;
	for (i = 0; i < parent.children.length; i++){
        if (isChildElement(parent.children[i],child)) return true;
    }
	return false;
}

gPt=function(e){if(parentn=e.parentNode,null==parentn||'HTML'==e.tagName)return'';if(e===document.body||e===document.head)return'/'+e.tagName;for(var t=0,a=e.parentNode.childNodes,n=0;n<a.length;n++){var r=a[n];if(r===e)return gPt(e.parentNode)+'/'+e.tagName+'['+(t+1)+']';1===r.nodeType&&r.tagName===e.tagName&&t++}};

function getElementsByXPath(xpath, parent){let results = [];let query = document.evaluate(xpath,parent || document,null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);for (let i=0, length=query.snapshotLength; i<length; ++i) {results.push(query.snapshotItem(i));}return results;}

getDescendants = function(node, accum) {
    var i;
    accum = accum || [];
    for (i = 0; i < node.childNodes.length; i++) {
        accum.push(node.childNodes[i]);
        getDescendants(node.childNodes[i], accum);
    }
    return accum;
}

var get_dompath=function(e){
    if(parentn=e.parentNode,null==parentn||'HTML'==e.tagName)return'';if(e===document.body||e===document.head)return'/'+e.tagName;for(var t=0,a=e.parentNode.childNodes,n=0;n<a.length;n++){var r=a[n];if(r===e)return get_dompath(e.parentNode)+'/'+e.tagName+'['+(t+1)+']';1===r.nodeType&&r.tagName===e.tagName&&t++}
};
var get_element_src=function(el){
    return el.outerHTML.split(">")[0]+">";
}

var get_element_full_src=function(el){
    return el.outerHTML;
}

var get_password_forms=function(){
    var forms = document.getElementsByTagName("form");
    password_forms = [];
    for (let form of forms){
        for (let child of getDescendants(form)){
            var nodetag = child.nodeName.toLowerCase();
            var etype = child.type;
            if (nodetag == "input" && etype == "password"){
                password_forms.push([form, '//html'+get_dompath(form)]);
            }
        }
    }
    return password_forms;
}

var get_local_storage = function(){
    var storage = {};
    for(var i = 0; i < localStorage.length; i++){
        var key = localStorage.key(i);
        storage[key] = localStorage[key];
    } return storage;
}

var get_session_storage = function(){
    var storage = {};
    for(var i = 0; i < sessionStorage.length; i++){
        var key = sessionStorage.key(i);
        storage[key] = sessionStorage[key];
    } return storage;
}

var get_attributes = function(el){
    var items = {};
    for (index = 0; index < arguments[0].attributes.length; ++index){
        items[arguments[0].attributes[index].name] = arguments[0].attributes[index].value
    } return items;
}

// obfuscate password input field
var change_password_type = function(){
    var forms = document.getElementsByTagName("form");
    for (let form of forms){
        for (let child of getDescendants(form)){
            var nodetag = child.nodeName.toLowerCase();
            var etype = child.type;
            if (nodetag == "input" && etype == "password"){
                child.type = "passw0rd";
            }
        }
    }
}

var form_obfuscation = function () {
    SIGNUP = "sign([^0-9a-zA-Z]|\s)*up|regist(er|ration)?|(create|new)([^0-9a-zA-Z]|\s)*(new([^0-9a-zA-Z]|\s)*)?(acc(ount)?|us(e)?r|prof(ile)?)";
    LOGIN = "(log|sign)([^0-9a-zA-Z]|\s)*(in|on)|authenticat(e|ion)|/(my([^0-9a-zA-Z]|\s)*)?(user|account|profile|dashboard)";

    var forms = document.getElementsByTagName("form");
    for (let form of forms){
        if (!onTopLayer(form)){
            continue;
        }
        for (let child of getDescendants(form)){
            var el_src = get_element_full_src(child);
            var nodetag = child.nodeName.toLowerCase();
            var etype = child.type;
            if (nodetag == "button" || etype == "submit" || etype == "button" || etype == "image") {
                if (el_src.match(SIGNUP)) {
                    el_src = el_src.replaceAll(SIGNUP, 'new_acc0unt');
                    child.outerHTML = el_src;
                }
                if (el_src.match(LOGIN)) {
                    el_src = el_src.replaceAll(LOGIN, 'l0gin');
                    child.outerHTML = el_src;
                }
            }
        }
        // check if form contains keywords
        form_src = get_element_src(form);
        if (form_src.match(SIGNUP) != null) {
            form_src = form_src.replaceAll(SIGNUP, 'new_acc0unt');
            form.outerHTML = form_src;
        }
        if (form_src.match(LOGIN) != null) {
            form_src = form_src.replaceAll(LOGIN, 'l0gin');
            form.outerHTML = form_src;
        }
    }
}


// Returns a dictionary of the form {"login" : [...], "signup" : [...]}
// where each list contains the corresponding form WebElements
var get_account_forms = function(){
    SIGNUP = "sign([^0-9a-zA-Z]|\s)*up|regist(er|ration)?|(create|new)([^0-9a-zA-Z]|\s)*(new([^0-9a-zA-Z]|\s)*)?(acc(ount)?|us(e)?r|prof(ile)?)";
    LOGIN = "(log|sign)([^0-9a-zA-Z]|\s)*(in|on)|authenticat(e|ion)|/(my([^0-9a-zA-Z]|\s)*)?(user|account|profile|dashboard)";
    ret = {"login" : [], "signup" : []};
    var forms = document.getElementsByTagName("form");
    for (let form of forms) {
        if (!onTopLayer(form)){
            continue;
        }
        // ret["login"].push("//html"+get_dompath(form))
        // continue;
        var inputs = 0;
        var hidden_inputs = 0;
        var passwords = 0;
        var hidden_passwords = 0;

        var checkboxes_and_radio = 0;
        var checkboxes = 0;
        var radios = 0;

        var signup_type_fields = 0;

        var login_submit = false;
        var signup_submit = false;

        for (let child of getDescendants(form)){
            var nodetag = child.nodeName.toLowerCase();
            var etype = child.type;
            var el_src = get_element_full_src(child);
            if (nodetag == "button" || etype == "submit" || etype == "button" || etype == "image"){
                if(el_src.match(SIGNUP)) signup_submit = true;
                if(el_src.match(LOGIN)) login_submit = true;
            }
            if (nodetag != "input" && nodetag != "select" && nodetag != "textarea") continue;
            if (etype == "submit" || etype == "button" || etype == "hidden" || etype == "image" || etype == "reset") continue;
            if (etype == "password"){
                if(onTopLayer(child)) passwords += 1;
                else hidden_passwords += 1;
            }else if(etype == "checkbox" || etype == "radio"){ // count them as well, but not as inputs
                if(etype == "checkbox") checkboxes += 1;
                else radios += 1;
                checkboxes_and_radio += 1;
            }else{
                if(onTopLayer(child)) inputs +=1;
                else hidden_inputs += 1;
                if(etype == "tel" || etype == "date" || etype == "datetime-local" || etype == "file" || etype == "month" || etype == "number" || etype == "url" || etype == "week"){
                    signup_type_fields += 1;
                }
            }
        }
        var total_inputs = inputs + hidden_inputs;
        var total_passwds = passwords + hidden_passwords;
        var total_visible = inputs + passwords;
        var total_hidden = hidden_inputs + hidden_passwords;

        if (total_passwds == 0){
            continue; // irrelevant form
        }
        // ret["login"].push("//html"+get_dompath(form))

        var signup = false;
        var login = false;

        reason = null;

        form_src = get_element_src(form);
        if (total_passwds > 1) {signup = true; reason = 1;}
        else{ // Only one password field
            if(login_submit && !signup_submit) {login = true; reason = 2;} // Only one should match, otherwise, rely on structure
            else if(signup_submit && !login_submit) {signup = true; reason = 3;}
            else if(total_inputs == 1) {login = true; reason = 4;}
            else {
                if (signup_type_fields > 0) {signup = true; reason = 5;}
                else{
                    if (inputs > 1) {signup = true; reason = 6; }// more than one visible inputs
                    else if (inputs == 1) {login = true; reason = 7;}
                    else{ // no visible inputs
                        if (form_src.match(SIGNUP) != null) {signup = true; reason = 8;}
                        else if (form_src.match(LOGIN) != null) {login = true; reason = 9;}
                        else {signup = true; reason = 9;} // no luck with regexes
                    }
                }
            }
        }

        if(passwords == 1 && form.className == "xenForm"){ // freaking xenforms
            signup = false;
            login = true;
        }

        if(signup){
            ret["signup"].push([form, '//html'+get_dompath(form)])
            console.log(reason)
            // ret["signup"].push(reason)
        }else if(login){
            ret["login"].push([form, '//html'+get_dompath(form)])
            console.log(reason)
            // ret["login"].push(reason)
        }
    }
    return ret;
};


console.log("XDriver lib is setup!");
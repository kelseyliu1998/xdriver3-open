**General**
-

XDriver was initially implemented as part of our project:

**_The Cookie Hunter: Automated Black-box Auditing for Web Authentication and Authorization Flaws_, CCS '20**

More information can be found [here](https://cookiehunter.ics.forth.gr/).

If you use XDriver for your research please [cite](https://cookiehunter.ics.forth.gr/data/bibtex.bib) our paper.

**Overview**
-

This tool was implemented to ease and enhance interaction with websites, as well as to provide a number of auxiliary mechanisms and a series of modules focused on security-related tasks. Some of the main features are:
- **Generic browser configuration** [(example)](https://gitlab.com/kostasdrk/xdriver3-open/-/tree/master/testing/browser_config.py)
    - Cross-browser support -- Chrome, Firefox, Opera
    - High-level abstractions for browser arguments -- E.g. supress SSL errors, disable notifications etc.
    - Virtual display -- Useful for systems without a physical monitor
- **Robustness**
    - Stale element handling -- Transparently tries to restore elements that became stale due to various reasons (e.g. redirections, crashes) [(example)](https://gitlab.com/kostasdrk/xdriver3-open/-/blob/master/testing/stale_elements.py)
    - Crash recovery -- Transparently restores the browser instance in case of a crash and maintains state
    - Retry -- Retries an operation that was interrupted by an exception that was handled (e.g. a browser crash)
- **Auxiliary functionality**
    - Configurable crawler -- Simple built-in crawler with several options, such as crawl depth, BFS/DFS, list of regexes for URLs to (not) follow etc. [(example)](https://gitlab.com/kostasdrk/xdriver3-open/-/tree/master/testing/crawler.py)
    - Form filler -- Given a form element, it tries to assign labels to each input element and based on those assign a likely appropriate value using the [Faker](https://faker.readthedocs.io/en/master/) package [(example)](https://gitlab.com/kostasdrk/xdriver3-open/-/tree/master/testing/formfill.py)
    - Misc -- More auxiliary methods, e.g. collect all third-party scripts, get all account related forms etc.
- **Internal proxy** -- Exposes functionality directly to the user, but is also used internally by XDriver
    - Simplify proxy chaining -- If both the internal and a custom proxy are enabled, it will automatically configure the setup as such: **_browser -> internal proxy -> custom proxy_**
    - Redirection flow -- Given a domain, it will return its redirection flow from its HTTP towards the HTTPS endpoint, along with the HTTP request/response headers [(example)](https://gitlab.com/kostasdrk/xdriver3-open/-/tree/master/testing/flows.py)
    - Strip media -- Returns dummy responses for media file requests to reduce load times
    - Force cookies -- Given a list of cookies for a given domain, ensure that only these will be sent out in subsequent requests to that domain, even if other cookies are in the cookie jar. This is useful to avoid persistent cookies in the browser that keep respawning even if we delete them [(example)](https://gitlab.com/kostasdrk/xdriver3-open/-/tree/master/testing/force_cookies.py)
    - Spoof request headers -- Self explanatory [(example)](https://gitlab.com/kostasdrk/xdriver3-open/-/tree/master/testing/spoofing.py)
- **Security policies evaluation** -- Evaluate a series of security mechanisms and return structured, parsable results on their deployment, detected misconfigurations, syntax errors etc. So far the following are supported: `HSTS, CSP, CORS, X-Content-Type-Options, X-XSS-Protection, X-Frame-Options, Expect-CT, Feature-Policy, Referrer-Policy` [(example)](https://gitlab.com/kostasdrk/xdriver3-open/-/tree/master/testing/sec.py)
**Acks & disclaimer**: In this version of XDriver, most of the security mechanisms evaluation methods have been implemented by Alex Savvopoulos (besides CORS). They have not been fully tested and evaluated and should be considered **experimental**.

**Setup**
-

Implemented and tested on Ubuntu 16.04 and 20.04 with python3.8. Should work on other debian-based systems as well.
* Clone the repo and `cd` into it
* `$ ./setup.sh`
* When prompted, download the webdriver binaries you need and place them under `./browsers/config/webdrivers`. If you want to do this at a later time, place them at that location in the install directory (e.g. `/usr/local/lib/python3.8/dist-packages/xdriver`)
* Make sure the webdrivers' versions are compatible with the corresponding browsers' version
* Run `./xutils/proxy/mitm/mitmdump` to create the `~/.mitmproxy` dir and add the generated mitmproxy certificate in the browsers' trust stores

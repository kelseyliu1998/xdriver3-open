#!/usr/bin/python

from .Logger import Logger
from time import time


class Timer():

	_caller_prefix = "ModuleTimer"

	def __init__(self, domain, module):
		self._domain = domain
		self._module = module
		self._start = time()

	def end(self, success = False, fail = False):
		self._total = time() - self._start
		return self._total
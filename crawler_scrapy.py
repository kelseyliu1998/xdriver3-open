
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
import os
import tldextract

class TestSpider(scrapy.Spider):
    name = "test"

    start_urls = ["https://"+x if not x.startswith("http") else x \
                  for x in open('460_urls.txt').readlines()]

    def parse(self, response):
        filename = response.url.split("://")[1].split('/')[0] + '.html'
        os.makedirs(os.path.join('1003_legitimate_contents', '460_legitimate_htmls'), exist_ok=True)
        with open(os.path.join('1003_legitimate_contents', '460_legitimate_htmls', filename), 'wb') as f:
            f.write(response.body)





